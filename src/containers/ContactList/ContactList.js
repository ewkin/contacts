import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchContacts, showContact, updateInitContact} from "../../store/actions/contactsAction";
import Modal from "../../components/UI/Modal/Modal";
import FullContact from "../FullContact/FullContact";


const ContactList = () => {
    const dispatch = useDispatch();
    const contacts = useSelector(state => state.contacts.contacts);
    const shouldUpdate = useSelector(state => state.contacts.updateState)
    const shouldShow = useSelector(state => state.contacts.showContact);


    useEffect(() => {
        if (shouldUpdate) {
            dispatch(fetchContacts())
            dispatch(updateInitContact(false));
        }

    }, [dispatch, shouldUpdate]);

    const contactCancelHandler = () => {
        dispatch(showContact(false, {}));
    };
    const contactShowHandler = contact => {
        dispatch(showContact(true, contact));
    };


    return (
        <>
            <Modal show={shouldShow}
                   closed={contactCancelHandler}
            >
                <FullContact/>
            </Modal>
            <div className="container">
                <div className="card card-default" id="card_contacts">
                    <div id="contacts" className="panel-collapse collapse show">
                        <ul className="list-group pull-down" id="contact-list">
                            {contacts.map(contact => (
                                <li onClick={() => contactShowHandler(contact)} key={contact.id}
                                    className="list-group-item">
                                    <div className="row w-100">
                                        <div className="col-12 col-sm-2 col-md-3 px-0">
                                            <img src={contact.url}
                                                 className="rounded-circle d-block img-fluid"
                                                 style={{width: '60%', height: 'auto'}} alt='Avatar'/>
                                        </div>
                                        <div className="col-12 col-sm-10 col-md-9 text-left text-sm-left">
                                            <p className='name lead'>{contact.name}</p>
                                        </div>
                                    </div>
                                </li>
                            ))}
                        </ul>
                    </div>
                </div>

            </div>
        </>
    );
};

export default ContactList;