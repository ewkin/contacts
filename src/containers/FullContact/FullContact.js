import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {deleteContact} from "../../store/actions/contactsAction";
import {NavLink} from "react-router-dom";

const FullContact = () => {
    const dispatch = useDispatch();
    const contact = useSelector(state => state.contacts.contact);

    return (
        <div className="container">
            <div className="card card-default" id="card_contacts">
                <div id="contacts" className="panel-collapse collapse show">
                    <div className="row w-100">
                        <div className="col-12 col-sm-6 col-md-3 px-0">
                            <img src={contact.url} alt={contact.name}
                                 className="rounded-circle mx-auto d-block img-fluid"
                                 style={{width: '50%', height: 'auto'}}/>
                        </div>
                        <div className="col-12 col-sm-6 col-md-9 text-center text-sm-left">
                            <span className="bi bi-file-earmark-person"/>
                            <label className="name lead">{contact.name}</label>
                            <br/>
                            <span className="bi bi-envelope"/>
                            <span className="text-muted">{contact.email}</span>
                            <br/>
                            <span className="bi bi-phone"/>
                            <span className="text-muted small">{contact.phone}</span>
                            <br/>
                            <div>
                                <NavLink to={"/edit-contact/" + contact.id}
                                         className="btn btn-info btn-block float-left rounded-0 py-2">
                                    Update
                                </NavLink>
                                <button onClick={() => dispatch(deleteContact(contact.id))}
                                        className="btn btn-danger btn-block float-right rounded-0 py-2">Delete
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default FullContact;