import React, {useState, useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {createContact, showContact, updateContact} from "../../store/actions/contactsAction";


const AddContact = props => {
    const dispatch = useDispatch();
    const contactToChange = useSelector(state => state.contacts.contact);

    const [contact, setContact] = useState({
        name: '',
        email: '',
        phone: '',
        url: '',
    });

    useEffect(() => {
        setContact(prevState => ({
            ...prevState,
            name: contactToChange.name,
            email: contactToChange.email,
            phone: contactToChange.phone,
            url: contactToChange.url
        }))
    }, [contactToChange]);

    const contactDataChanged = event => {
        const {name, value} = event.target;

        setContact(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const contactHandler = event => {
        event.preventDefault();
        props.history.push('/');
        if (Object.keys(contactToChange).length === 0) {
            dispatch(createContact(contact))

        } else {
            dispatch(updateContact(contact, contactToChange.id));
            dispatch(showContact(false, {}));
        }
    };
    let photo = null;
    if (contact.url) {
        photo = (<div className="form-group">
            <div className="input-group mb-2">
                <div className="input-group-text">Photo preview:
                </div>
                <img src={contact.url} style={{width: '20%', height: 'auto'}}
                     className="rounded mx-auto d-block" alt="Avatar"/>
            </div>
        </div>);
    }


    return (
        <div className="container">
            <h2 className="text-center">Contact Form</h2>
            <div className="row justify-content-center">
                <div className="col-12 col-md-8 col-lg-6 pb-5">
                    <form onSubmit={contactHandler}>
                        <div className="card border-primary rounded-0">
                            <div className="form-group">
                                <div className="input-group mb-2">
                                    <div className="input-group-prepend">
                                        <div className="input-group-text"><i className="bi bi-file-earmark-person"/>
                                        </div>
                                    </div>
                                    <input type="text" className="form-control" name="name" value={contact.name}
                                           onChange={contactDataChanged}
                                           placeholder="Name"/>
                                </div>
                            </div>
                            <div className="form-group">
                                <div className="input-group mb-2">
                                    <div className="input-group-prepend">
                                        <div className="input-group-text"><i className="bi bi-envelope"/>
                                        </div>
                                    </div>
                                    <input type="email" className="form-control" name="email" value={contact.email}
                                           onChange={contactDataChanged}
                                           placeholder="Email"/>
                                </div>
                            </div>
                            <div className="form-group">
                                <div className="input-group mb-2">
                                    <div className="input-group-prepend">
                                        <div className="input-group-text"><i className="bi bi-phone"/>
                                        </div>
                                    </div>
                                    <input type="tel" className="form-control" id="phone" name="phone"
                                           value={contact.phone} onChange={contactDataChanged}
                                           placeholder="Phone"/>
                                </div>
                            </div>
                            <div className="form-group">
                                <div className="input-group mb-2">
                                    <div className="input-group-prepend">
                                        <div className="input-group-text"><i className="bi bi-image-alt"/>
                                        </div>
                                    </div>
                                    <input type="text" className="form-control" id="photo" name="url"
                                           value={contact.url} onChange={contactDataChanged}
                                           placeholder="Photo"/>
                                </div>
                            </div>
                            {photo}
                            <div className="text-center">
                                <button type="submit" className="btn btn-info btn-block float-left rounded-0 py-2">Send
                                </button>
                                <button onClick={() => {
                                    props.history.push('/')
                                    dispatch(showContact(false, {}));
                                }}
                                        className="btn btn-danger btn-block float-right rounded-0 py-2">Cancel
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default AddContact;