import {axiosContacts} from "../../axios-contacts";


export const FETCH_CONTACT_REQUEST = 'FETCH_CONTACT_REQUEST';
export const FETCH_CONTACT_SUCCESS = 'FETCH_CONTACT_SUCCESS';
export const FETCH_CONTACT_FAILURE = 'FETCH_CONTACT_FAILURE';
export const INIT_CONTACT = 'INIT_CONTACT';
export const UPDATE_INIT_CONTACT = 'UPDATE_INIT_CONTACT';
export const SHOW_CONTACT = 'SHOW_CONTACT';

export const fetchContactRequest = () => ({type: FETCH_CONTACT_REQUEST});
export const fetchContactSuccess = () => ({type: FETCH_CONTACT_SUCCESS});
export const fetchContactFailure = error => ({type: FETCH_CONTACT_FAILURE, error});
export const initContact = contacts => ({type: INIT_CONTACT, contacts})
export const updateInitContact = value => ({type: UPDATE_INIT_CONTACT, value: value});
export const showContact = (value, contact) => ({type: SHOW_CONTACT, value: value, contact: contact});

export const fetchContacts = () => {
    return async dispatch => {
        try {
            dispatch(fetchContactRequest());
            const response = await axiosContacts.get('/contacts.json');
            const contacts = Object.keys(response.data).map(id => ({
                ...response.data[id], id
            }))
            dispatch(fetchContactSuccess());
            dispatch(initContact(contacts));
        } catch (error) {
            dispatch(fetchContactFailure(error));
        }
    };
};

export const createContact = contact => {
    return async dispatch => {
        try {
            dispatch(fetchContactRequest());
            await axiosContacts.post('/contacts.json', contact);
            dispatch(fetchContactSuccess());
            dispatch(updateInitContact(true));
        } catch (error) {
            dispatch(fetchContactFailure(error));
        }
    };
};

export const deleteContact = id => {
    return async dispatch => {
        try {
            dispatch(fetchContactRequest());
            await axiosContacts.delete('/contacts/' + id + '.json');
            dispatch(fetchContactSuccess());
            dispatch(updateInitContact(true));
            dispatch(showContact(false, {}));
        } catch (error) {
            dispatch(fetchContactFailure(error));
        }
    };
};

export const updateContact = (contact, id) => {
    return async dispatch => {
        try {
            dispatch(fetchContactRequest());
            await axiosContacts.put('/contacts/'+id+'.json', contact);
            dispatch(fetchContactSuccess());
            dispatch(updateInitContact(true));
        } catch (error) {
            dispatch(fetchContactFailure(error));
        }
    };
};