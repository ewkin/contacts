import axios from 'axios';

export const axiosContacts = axios.create({
    baseURL: 'https://contact-list-9acfe-default-rtdb.firebaseio.com/'
});
