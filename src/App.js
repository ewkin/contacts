import React from 'react';
import {Route, Switch} from "react-router-dom";

import Layout from "./components/Layout/Layout";
import ContactList from "./containers/ContactList/ContactList";
import AddContact from "./containers/AddContact/AddContact";

const App = () => {
    return (
        <Layout>
            <Switch>
                <Route path="/" exact component={ContactList}/>
                <Route path="/new-contact" component={AddContact}/>
                <Route path="/edit-contact/:id" component={AddContact}/>
                <Route render={() => <h1>Not found</h1>} />
            </Switch>
        </Layout>
    );
};

export default App;