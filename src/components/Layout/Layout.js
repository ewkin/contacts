import React from 'react';
import './Layout.css';
import NavList from "../NavList/NavList";
const Layout = props => {
    return (
        <>
            <header className="Toolbar">
                <div className="Toolbar-logo">
                    <div className="Logo">
                        Contacts
                    </div>
                </div>
                <nav>
                    <NavList/>
                </nav>
            </header>
            <main className="Layout-Content">
                {props.children}
            </main>
        </>
    );
};

export default Layout;