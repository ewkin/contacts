import React from 'react';
import './NavItem.css';
import {NavLink} from "react-router-dom";

const NavItem = ({to, exact, children}) => (
    <li className="NavigationItem">
        <NavLink to={to} exact={exact}>
            {children}
        </NavLink>
    </li>
);

export default NavItem;